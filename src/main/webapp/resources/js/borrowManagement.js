/**
 *  function used to verify if the borrower is not empty
 */
function verify(){
	let borrower = document.getElementById("borrower").value;
	let select = document.getElementById("id");
	let select1 = document.getElementById("id_borrow");
	
	if(select.value == " " || !borrower.match("[a-zA-Z_ ]{3,20}")) {
		document.getElementById("borrow_submit").disabled = true;
	} else {
		document.getElementById("borrow_submit").disabled = false;
	}
	
	if(select1.value == " ") {
		document.getElementById("return_submit").disabled = true;
	} else {
		document.getElementById("return_submit").disabled = false;
	}
}

verify();
setInterval(verify,500);