
/**
 *  function used to verify book form fields
 */
function verify(){
	let title = document.getElementById("title").value;
	let editor = document.getElementById("editor").value;
	let authorFirstname = document.getElementById("authorFirstname").value;
	let authorLastname = document.getElementById("authorLastname").value;
	let year = document.getElementById("year").value;
	
	let titleSpan = document.getElementById("titleSpan");
	let editorSpan = document.getElementById("editorSpan");
	let authorFirstnameSpan = document.getElementById("authorFirstnameSpan");
	let authorLastnameSpan = document.getElementById("authorLastnameSpan");
	let yearSpan = document.getElementById("yearSpan");
	
	// Active or disable submit when fields are correct or wrong
	if((title.match("[a-zA-Z0-9_ ]{3,50}"))
		&&(editor.match("[a-zA-Z0-9_ ]{3,20}"))
		&&(authorFirstname.match("[a-zA-Z_ ]{3,20}"))
		&&(authorLastname.match("[a-zA-Z_ ]{3,20}"))
		&&(year >= 1)
		&&(year <= 2022)
		){
		document.getElementById("book_submit").disabled = false;
	} else {
		document.getElementById("book_submit").disabled = true;
		
		//Title error span
		if(!title.match("[a-zA-Z0-9_ ]{3,50}") && title != ""){
			titleSpan.textContent = "Entrez un titre valide";
		} else {
			titleSpan.textContent = "";
		}
		
		//Editor error span
		if(!editor.match("[a-zA-Z0-9_ ]{3,50}") && editor != ""){
			editorSpan.textContent = "Entrez un editeur valide";
		} else {
			editorSpan.textContent = "";
		}
		
		//Author firstname error span
		if(!authorFirstname.match("[a-zA-Z0-9_ ]{3,50}") && authorFirstname != ""){
			authorFirstnameSpan.textContent = "Entrez un prénom valide";
		} else {
			authorFirstnameSpan.textContent = "";
		}
		
		//Author lastname error span
		if(!authorLastname.match("[a-zA-Z0-9_ ]{3,50}") && authorLastname != ""){
			authorLastnameSpan.textContent = "Entrez un nom valide";
		} else {
			authorLastnameSpan.textContent = "";
		}
		
		//Year error span
		if((year < 1 || year > 2022) && (year != "") && (year.match("[0-9]{1,4}"))){
			yearSpan.textContent = "Entrez une année valide";
		} else {
			yearSpan.textContent = "";
		}
	}
}

setInterval(verify,500);