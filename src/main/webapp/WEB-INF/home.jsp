<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
	<link href="<c:url value="/resources/css/style_mobile.css"/>" rel="stylesheet" media="screen and (max-width: 425px)">
	<meta charset="UTF-8">
	<title>Accueil</title>
</head>
<body>
	<header>
		<h1>Bibliothèque</h1>
		<nav>
			<a href="/">Accueil</a> | <a href="/borrows">Emprunts</a>
		</nav>
	</header>
	
	<div class="main-container">
		<div class="left-container">
			<img alt="The Royal Library" src="/resources/img/bibliotheque.jpg">
		</div>
		<div class="right-container">
			<h2>Ajouter un livre</h2>
			<form class="homeForm" method="post">
				<div class="tables">
					<label for="title">Titre</label>
					<input type="text" id="title" name="title" placeholder="Entrez un titre">
				</div>
				<span id="titleSpan"></span>
				<div class="tables">
					<label for="year">Parution</label>
					<input type="text" id="year" name="year" placeholder="Entrez l'année de parution">
				</div>
				<span id="yearSpan"></span>
				<div class="tables">
					<label for="editor">Editeur</label>
					<input type="text" id="editor" name="editor" placeholder="Entrez un éditeur">
				</div>
				<span id="editorSpan"></span>
				<div class="tables">
					<label for="authorFirstname">Prénom</label>
					<input type="text" id="authorFirstname" name="authorFirstname" placeholder="Entrez le prénom de l'auteur">
				</div>
				<span id="authorFirstnameSpan"></span>
				<div class="tables">
					<label for="authorLastname">Nom</label>
					<input type="text" id="authorLastname" name="authorLastname" placeholder="Entrez le nom de l'auteur">
				</div>
				<span id="authorLastnameSpan"></span>
				<input id="book_submit" type="submit" value="Enregistrer">
			</form>
		</div>
	</div>
    <footer>
        <div class="inner-footer">
        <a href="/mock">&copy;</a>
        	<a href="/additional-features">Groupe 4</a>
        </div>
        <div class="inner-footer">
        	<a href="https://www.ldnr.fr/" target="blank">LDNR</a> |
        	<a href="https://www.linkedin.com/in/hakim-ben-s/" target="blank">Hakim Ben Slama</a> | 
        	<a href="https://www.linkedin.com/in/jonathan-esterhazy-538a88b/" target="blank">Jonathan Esterhazy</a> | 
			<a href="https://www.linkedin.com/in/florian-lecocq/" target="blank">Florian Lecocq</a> | 
			<a href="https://www.linkedin.com/in/annelise-ostre/" target="blank">Annelise Ostré</a>
		</div>
    </footer>
	<script src="<c:url value="/resources/js/home.js"/>"></script>	
</body>
</html>