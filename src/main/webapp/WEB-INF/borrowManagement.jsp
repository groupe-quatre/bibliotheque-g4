<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/css/style_mobile.css" />" rel="stylesheet" media="screen and (max-width: 425px)">
	<meta charset="UTF-8">
	<title>Emprunts</title>
</head>
<body>
	<header>
		<h1>Bibliothèque</h1>
		<nav>
			<a href="/">Accueil</a> | <a href="/borrows">Emprunts</a>
		</nav>
	</header>

<div class="borrow-full-container">
	<h2>Gestion des emprunts</h2>
	<div class="borrow-container">		
		<div class="container">
			<form action="/create-borrow" method="post">
				<h3>Emprunts</h3>
				<div class="borrow-tables">
					<select name="id" id="id">
					<option value=" "> - Choisir le livre à emprunter - </option>
						<c:forEach items="${books}" var="b">
							<c:if test="${b.status == true }">
								<option value="${b.id}">${b.id} -- ${b.title} -- ${b.authorFirstname} ${b.authorLastname}</option>
							</c:if>
						</c:forEach>
					</select>
					<input type="text" id="borrower" name="borrower" placeholder="Entrez le nom de l'emprunteur"> 
				</div>
				<input id="borrow_submit" type="submit" value="Enregistrer">	
			</form>
		</div>
		<div class="container">
			<form action="/return-borrow" method="post">
				<h3>Retours</h3>
				<div class="borrow-tables">
					<select name="id_borrow" id="id_borrow">
					<option value=" "> - Choisir le livre de retour - </option>
					<c:forEach items="${borrows}" var="borrow">
					<c:if test="${borrow.returnDate == null}">
						<option value="${borrow.id}">
							${borrow.book_id} -- 
							<c:forEach items="${books}" var="book">
							<c:if test="${borrow.book_id == book.id}">
							${book.title} -- 
							${book.authorFirstname} -- 
							${book.authorLastname} -- 
							</c:if>
							</c:forEach>
							${borrow.borrowDate} -- 
							${borrow.borrower}
						</option>
					</c:if>
					</c:forEach>
					</select> 
				</div>
				<input id="return_submit" type="submit" value="Enregistrer">
			</form>
		</div>
	</div>
	
	<div class="borrow-tables">
		<div class="inner-table">
			<h3>Livres disponibles</h3>
			<table>
				<thead>
					<tr>
						<th>Id</th>
						<th>Titre</th>
						<th>Parution</th>
						<th>Editeur</th>
						<th>Prénom</th>
						<th>Nom</th>
					</tr>
				</thead>
				<c:forEach items="${books}" var="b">
					<c:if test="${b.status == true }">
					<tr>
						<td>${b.id}</td>
						<td>${b.title}</td>
						<td>${b.year}</td>
						<td>${b.editor}</td>
						<td>${b.authorFirstname}</td>
						<td>${b.authorLastname}</td>
					</tr></c:if>
				</c:forEach>
			</table>
		</div>
		<div class="inner-table">
			<h3>Locations en cours</h3>
			<table>
				<thead>
					<tr>
						<th>Id emprunt</th>
						<th>Id livre</th>
						<th>Titre</th>
						<th>Prénom</th>
						<th>Nom</th>
						<th>Date d'emprunt</th>
						<th>Emprunteur</th>
					</tr>
				</thead>
				<c:forEach items="${borrows}" var="borrow">
				<c:if test="${borrow.returnDate == null}">
					<tr>
						<td>${borrow.id}</td>
						<td>${borrow.book_id}</td>
						<c:forEach items="${books}" var="book">
						<c:if test="${borrow.book_id == book.id}">
						<td>${book.title}</td>
						<td>${book.authorFirstname}</td>
						<td>${book.authorLastname} </td>
						</c:if>
						</c:forEach>
						<td>${borrow.borrowDate}</td>
						<td>${borrow.borrower}</td>
					</tr>
				</c:if>
				</c:forEach>
			</table>
		</div>
	</div>
</div>
<footer>
	<div class="inner-footer">
		<a href="/additional-features">&copy; Groupe 4</a>
	</div>
	<div class="inner-footer">
		<a href="https://www.ldnr.fr/" target="blank">LDNR</a> |
      	<a href="https://www.linkedin.com/in/hakim-ben-s/" target="blank">Hakim Ben Slama</a> | 
      	<a href="https://www.linkedin.com/in/jonathan-esterhazy-538a88b/" target="blank">Jonathan Esterhazy</a> | 
		<a href="https://www.linkedin.com/in/florian-lecocq/" target="blank">Florian Lecocq</a> | 
		<a href="https://www.linkedin.com/in/annelise-ostre/" target="blank">Annelise Ostré</a>
	</div>
</footer>
<script src="<c:url value="/resources/js/borrowManagement.js"/>"></script>	
</body>
</html>

