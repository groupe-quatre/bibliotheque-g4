<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
	<link href="<c:url value="/resources/css/style_mobile.css"/>" rel="stylesheet" media="screen and (max-width: 425px)">
	<meta charset="UTF-8">
	<title>Fonctionnalités supplémentaires</title>
</head>
<body>
	<header>
		<h1>Bibliothèque</h1>
		<nav>
			<a href="/">Accueil</a> | <a href="/borrows">Emprunts</a>
		</nav>
	</header>

	<div class="additional-full-container">
		<h2>Gestion des livres</h2>
		<div class="additional-container">
			<div class="container">
				<form action="/delete-book" method="post">
					<h3>Suppression des livres</h3>
					<div class="tables">
						<label for="book_id">Choisissez le livre à supprimer</label> 
						<select name="book_id" id="book_id">
							<c:forEach items="${books}" var="b">
								<option value="${b.id}">${b.id} -- ${b.title} -- ${b.authorFirstname} -- ${b.authorLastname}</option>
							</c:forEach>
						</select>
					</div>
					
					<input id="submit" type="submit" value="Supprimer">
				</form>
			</div>
			<div class="container">
				<form action="/update-book" method="post">
					<h3>Mise à jour des livres</h3>
					<div class="tables">
						<label for="book_update_id">Choisissez le livre à modifier</label> 
						<select name="book_update_id" id="book_update_id">
							<c:forEach items="${books}" var="b">
								<option value="${b.id}">${b.id} -- ${b.title} -- ${b.year} -- ${b.editor} -- ${b.authorFirstname} -- ${b.authorLastname}</option>
							</c:forEach>
						</select>
					</div>
					
					<div class="tables">
						<label for="title">Titre</label> 
						<input type="text" id="title" name="title" placeholder="Entrez le titre">
					</div>
					<span id="titleSpan"></span>
		
		
					<div class="tables">
						<label for="parution">Parution</label> 
						<input type="text" id="year" name="year" placeholder="Entrez l'année de la parution">
					</div>
					<span id="yearSpan"></span>
		
					<div class="tables">
						<label for="editor">Editeur</label> 
						<input type="text" id="editor" name="editor" placeholder="Entrez le nom de l'éditeur">
					</div>
					<span id="editorSpan"></span>
					
					<div class="tables">
						<label for="authorFirstname">Prénom de l'auteur</label> 
						<input type="text" id="authorFirstname" name="authorFirstname" placeholder="Entrez le prénom de l'auteur">
					</div>
					<span id="authorFirstnameSpan"></span>
					
					<div class="tables">
						<label for="authorLastname">Nom de l'auteur</label> 
						<input type="text" id="authorLastname" name="authorLastname" placeholder="Entrez le nom de l'auteur">
					</div>
					<span id="authorLastnameSpan"></span>
					
					<input id="book_submit" type="submit" value="Modifier">
				</form>
			</div>
		</div>
		<h2>Gestion des emprunts</h2>
		<div class="additional-container">
			<div class="container">
				<form action="/delete-borrow" method="post">
					<h3>Suppression des emprunts</h3>
					<div class="tables">
						<label for="borrow_id">Choisissez l'emprunt à supprimer</label> 
						<select name="borrow_id" id="borrow_id">
							<c:forEach items="${borrows}" var="b">
								<option value="${b.id}">${b.id} -- ${b.book_id} -- ${b.borrower} -- ${b.borrowDate} -- ${b.returnDate}</option>
							</c:forEach>
						</select>
					</div>
					<input id="submit_delete_borrow" type="submit" value="Supprimer">
				</form>
			</div>
			<div class="container">
				<form action="/update-borrow" method="post">
					<h3>Mise à jour des emprunts</h3>
					<div class="tables">
						<label for="borrow_id">Choisissez l'emprunt à modifier</label> 
						<select name="borrow_id" id="borrow_id">
							<c:forEach items="${borrows}" var="b">
								<option value="${b.id}">${b.id} -- ${b.book_id} -- ${b.borrower} -- ${b.borrowDate} -- ${b.returnDate}</option>
							</c:forEach>
						</select>
					</div>
					<div class="tables">
						<label for="borrower">Emprunteur</label> 
						<input type="text" id="borrower" name="borrower" placeholder="Entrez le nom de l'emprunteur">
					</div>
					
					<div class="tables">
						<label for="returnDate">Date de retour</label> 
						<input type="date" min="${b.borrowDate}" max="${today}" id="returnDate" name="returnDate" placeholder="Entrez la date de retour">
					</div>
					
					<input id="borrow_submit" type="submit" value="Modifier">
				</form>
			</div>
		</div>

	</div>

	<footer>
		<div class="inner-footer">
			<a href="/additional-features">&copy; Groupe 4</a>
		</div>
		<div class="inner-footer">
			<a href="https://www.ldnr.fr/" target="blank">LDNR</a> |
	      	<a href="https://www.linkedin.com/in/hakim-ben-s/" target="blank">Hakim Ben Slama</a> | 
	      	<a href="https://www.linkedin.com/in/jonathan-esterhazy-538a88b/" target="blank">Jonathan Esterhazy</a> | 
			<a href="https://www.linkedin.com/in/florian-lecocq/" target="blank">Florian Lecocq</a> | 
			<a href="https://www.linkedin.com/in/annelise-ostre/" target="blank">Annelise Ostré</a>
		</div>
	</footer>

	<script src="<c:url value="/resources/js/home.js"/>"></script>
	<script src ="<c:url value="/resources/js/borrowManagement.js"/>"></script>
</body>
</html>