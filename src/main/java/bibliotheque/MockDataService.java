/**
 * 
 */
package bibliotheque;

import java.time.LocalDate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian lcq 
 * created 27 June 2022
 */
@Service
public class MockDataService {
	private SessionFactory s;
	
	public void createMockDataBook() {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		Book b = new Book("1984", 1949, "Gallimard" , "George" , "Orwell");
		Book b1 = new Book("L'île mystérieuse", 2002, "Le livre de poche" , "Jules" , "Verne");
		Book b2 = new Book("20 000 lieues sous les mers", 1990, "Le livre de poche" , "Jules" , "Verne");
		Book b3 = new Book("Téméraire", 2006, "France Loisirs" , "Naomi" , "Novik");
		Book b4 = new Book("Troie", 2005, "France Loisirs" , "David" , "Gemmell");
		Book b5 = new Book("Le trône de fer", 1998, "France Loisirs" , "GRR" , "Martin");
		Book b6 = new Book("Misery", 1987, "J'ai lu" , "Stephen" , "King");
		session.save(b);
		session.save(b1);
		session.save(b2);
		session.save(b3);
		session.save(b4);
		session.save(b5);
		session.save(b6);
		t.commit();
		session.close();
	}
	
	public void createMockDataBorrow() {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		Borrow b = new Borrow(1, "John Doe");
		Borrow b1 = new Borrow(3, "Jane Doe");
		Borrow b2 = new Borrow(5, "John Smith");
		Borrow b3 = new Borrow(7, "Jane Smith");
		LocalDate d = LocalDate.of(2020, 10, 25);
		LocalDate d1 = LocalDate.of(2022, 5, 8);
		LocalDate d2 = LocalDate.of(2022, 3, 11);
		LocalDate d3 = LocalDate.of(2020, 12, 2);
		LocalDate d4 = LocalDate.of(2021, 9, 18);
		b.setBorrowDate(d);
		b.setReturnDate(d3);
		b1.setBorrowDate(d1);
		b2.setBorrowDate(d2);
		b3.setBorrowDate(d4);
		session.save(b);
		session.save(b1);
		session.save(b2);
		session.save(b3);
		t.commit();
		session.close();
	}
	
	public void updateBookStatus() {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		Book b = session.load(Book.class, 3);
		Book b1 = session.load(Book.class, 5);
		Book b2 = session.load(Book.class, 7);
		b.setStatus(false);
		b1.setStatus(false);
		b2.setStatus(false);
		t.commit();
		session.close();
	}
	

	@Autowired
	public void setSessionFactory(SessionFactory session) {
		this.s = session;
	}
}
