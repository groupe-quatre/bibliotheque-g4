/**
 * 
 */
package bibliotheque;

import java.util.Properties;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;




/**
 * @author Florian lcq 
 * @author Hakim
 * @author Jonathan
 * created 27 juin 2022
 */
@SpringBootApplication
public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		System.out.println("\n╔╗─╔╦═══╦╗──╔╗──╔═══╗╔╗\n║║─║║╔══╣║──║║──║╔═╗║║║\n║╚═╝║╚══╣║──║║──║║─║║║║\n║╔═╗║╔══╣║─╔╣║─╔╣║─║║╚╝\n║║─║║╚══╣╚═╝║╚═╝║╚═╝║╔╗\n╚╝─╚╩═══╩═══╩═══╩═══╝╚╝\n");
		System.out.println("Application available at: http://localhost:8080");
		}
	
	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver vr = new InternalResourceViewResolver();
		vr.setViewClass(JstlView.class);
		vr.setPrefix("/WEB-INF/");
		vr.setSuffix(".jsp");
		return vr;
	}
	
	@Bean
	public SessionFactory getSessionFactory() {
		Properties options = new Properties();
		options.put("hibernate.dialect", "org.sqlite.hibernate.dialect.SQLiteDialect");
		options.put("hibernate.connection.driver_class", "org.sqlite.JDBC");
		options.put("hibernate.connection.url", "jdbc:sqlite:bibliotheque_groupe4.sqlite");
		options.put("hibernate.hbm2ddl.auto", "create-drop");
		options.put("hibernate.show_sql", "true");
		SessionFactory sf = new Configuration().addProperties(options)
				.addAnnotatedClass(Book.class)
				.addAnnotatedClass(Borrow.class)
				.buildSessionFactory();
		return sf;
	}
}


