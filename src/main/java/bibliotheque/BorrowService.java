/**
 * 
 */
package bibliotheque;

import java.time.LocalDate;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian lcq
 * created 27 June 2022
 */
@Service
public class BorrowService {
	SessionFactory s;
	
	public void save(int id, String borrower) {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		Borrow b = new Borrow(id, borrower);
		b.setBorrowDate(LocalDate.now());
		Book book = session.load(Book.class, id);
		book.setStatus(false);
		session.save(b);
		session.update(book);
		t.commit();
		session.close();
	}
	
	public Borrow load(int id) {
		Session session = s.openSession();
		Borrow bo = session.load(Borrow.class, id);
		return bo;
	}
	
	public void updateBorrow(int id, String borrower, LocalDate returnDate) {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		Borrow b= session.load(Borrow.class, id);
		b.setBorrower(borrower);
		b.setReturnDate(returnDate);		
		session.update(b);
		t.commit();
		session.close();
	}
	
	public void removeBorrow(int id) {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		session.remove(session.load(Borrow.class, id));
		t.commit();
		session.close();
	}
	
	public void returnBorrow(int borrow_id) {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		Borrow b = session.load(Borrow.class, borrow_id);
		b.setReturnDate(LocalDate.now());
		Book book = session.load(Book.class, b.getBook_id());
		book.setStatus(true);
		session.update(b);
		session.update(book);
		t.commit();
		session.close();
	}
	
	public List<Borrow> readBorrows(){
		Session session = s.openSession();
		List<Borrow> list = session
				.createQuery("from Borrow", Borrow.class)
				.list();
		session.close();
		return list;
	}
	
	public List<Borrow> readBorrowsByDate(){
		Session session = s.openSession();
		List<Borrow> list = session
				.createQuery("from Borrow ORDER BY borrowDate DESC", Borrow.class)
				.list();
		session.close();
		return list;
	}
	
	public List<Borrow> readCurrentBorrows(){
		Session session = s.openSession();
		List<Borrow> list = session
				.createQuery("from Borrow WHERE returnDate = null ORDER BY borrowDate DESC", Borrow.class)
				.list();
		session.close();
		return list;
	}
	
	@Autowired
	public void setSessionFactory(SessionFactory session) {
		this.s = session;
	}
}
