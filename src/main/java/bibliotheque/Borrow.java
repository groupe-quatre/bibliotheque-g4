/**
 * 
 */
package bibliotheque;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Florian lcq
 * @author Annelise OSTRÉ 
 * created 27 juin 2022
 */

@Entity
public class Borrow {
	private int id;
	private int book_id;
	private String borrower;
	private LocalDate borrowDate;
	private LocalDate returnDate;

	public Borrow() {
		this(0, null);
		}
	
	@SuppressWarnings("static-access")
	public Borrow(int book_id, String borrower) {
		this.id = 0;
		this.book_id = book_id;
		this.borrower = borrower;
		this.borrowDate = borrowDate.now();
		this.returnDate = null;
	}

	/**
	 * @return the id
	 */
	@Column(name = "identifiant")
	@Id // indexé et unique
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the book_id
	 */
	@Column(nullable = false)
	public int getBook_id() {
		return book_id;
	}

	/**
	 * @param book_id the book_id to set
	 */
	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}

	/**
	 * @return the borrower
	 */
	@Column(length = 50, nullable = false)
	public String getBorrower() {
		return borrower;
	}

	/**
	 * @param borrower the borrower to set
	 */
	public void setBorrower(String borrower) {
		this.borrower = borrower;
	}

	/**
	 * @return the borrowDate
	 */
	@Column(nullable = false)
	public LocalDate getBorrowDate() {
		return borrowDate;
	}

	/**
	 * @param borrowDate the borrowDate to set
	 */
	public void setBorrowDate(LocalDate borrowDate) {
		this.borrowDate = borrowDate;
	}

	/**
	 * @return the returnDate
	 */
	@Column(nullable = true)
	public LocalDate getReturnDate() {
		return returnDate;
	}

	/**
	 * @param returnDate the returnDate to set
	 */
	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}

	@Override
	public String toString() {
		return "Borrow id:" + id + ", book_id:" + book_id + ", borrower:" + borrower + ", borrowDate:" + borrowDate
				+ ", returnDate=" + returnDate;
	}

}
