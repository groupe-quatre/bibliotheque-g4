/**
 * 
 */
package bibliotheque;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Annelise
 * @author Hakim
 * @author Jonathan
 * @author Florian lcq 
 * created 27 June 2022
 */
@Service
public class BookService {
	private SessionFactory s;

	public void save(String title, int year, String editor, String authorFirstname, String authorLastname) {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		session.save(new Book(title, year, editor, authorFirstname, authorLastname));
		t.commit();
		session.close();
	}

	public Book load(int id) {
		Session session = s.openSession();
		Book b = session.load(Book.class, id);
		return b;
	}

	public void updateBook(int id, String title, int year, String editor, String authorFirstname,
			String authorLastname) {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		Book b = session.load(Book.class, id);
		b.setTitle(title);
		b.setYear(year);
		b.setEditor(editor);
		b.setAuthorFirstname(authorFirstname);
		b.setAuthorLastname(authorLastname);
		session.update(b);
		t.commit();
		session.close();
	}

	public void removeBook(int id) {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		session.remove(session.load(Book.class, id));
		t.commit();
		session.close();
	}

	public void changeStatus(int id) {
		Session session = s.openSession();
		Transaction t = session.beginTransaction();
		Book b = session.load(Book.class, id);
		b.setStatus(!b.getStatus());
		session.update(b);
		t.commit();
		session.close();
	}

	public List<Book> readBooks() {
		Session session = s.openSession();
		List<Book> list = session.createQuery("from Book", Book.class).list();
		session.close();
		return list;
	}

	@Autowired
	public void setSessionFactory(SessionFactory session) {
		this.s = session;
	}
}
