/**
 * 
 */
package bibliotheque;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Florian lcq
 * created 27 juin 2022
 */

@Entity
public class Book {
	private int id;
	private String title;
	private int year;
	private String editor;
	private String authorFirstname;
	private String authorLastname;
	private Boolean status;
	
	
	public Book() {
		this(null, 0, null, null, null);
	}
	
	public Book(String title, int year, String editor, String authorFirstname, String authorLastname) {
		this.id = 0;
		this.title = title;
		this.year = year;
		this.editor = editor;
		this.authorFirstname = authorFirstname;
		this.authorLastname = authorLastname;
		this.status = true;
	}
	
	/**
	 * @return the id
	 */
	@Column(name = "identifiant")
	@Id // indexé et unique
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	@Column(length = 50, nullable = false)
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(length = 4,nullable = false)
	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	@Column(length = 50, nullable = false)
	public String getEditor() {
		return editor;
	}
	
	public void setEditor(String editor) {
		this.editor = editor;
	}
	
	@Column(length = 50, nullable = false)
	public String getAuthorFirstname() {
		return authorFirstname;
	}
	
	public void setAuthorFirstname(String authorFirstname) {
		this.authorFirstname = authorFirstname;
	}
	
	@Column(length = 50, nullable = true)
	public String getAuthorLastname() {
		return authorLastname;
	}
	
	public void setAuthorLastname(String authorLastname) {
		this.authorLastname = authorLastname;
	}
	
	@Column(nullable = false)
	public boolean getStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	

}
