/**
 * 
 */
package bibliotheque;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Annelise
 * @author Hakim
 * @author Jonathan
 * @author Florian lcq 
 * created 27 June 2022
 */
@Controller
public class MainController {
	private BookService bs;
	private BorrowService bos;
	private MockDataService mock;

	
	/**
	 * POST and GET on home page
	 */
	
	@RequestMapping({ "/home", "/" })
	private String showHome() {
		return "home";
	}
	

	@PostMapping({ "/home", "/" })
	private String saveBook(Model model, 
			@RequestParam("title") String title, 
			@RequestParam("year") String year,
			@RequestParam("editor") String editor, 
			@RequestParam("authorFirstname") String authorFirstname,
			@RequestParam("authorLastname") String authorLastname) {
		int yearToInt = Integer.parseInt(year);
		bs.save(title, yearToInt, editor, authorFirstname, authorLastname);
		return "home";
	}
	
	/*
	 * POST and GET on borrow page
	 */

	@RequestMapping({"/borrows"})
	private String showBorrows(Model model) {
		List<Book> list = bs.readBooks();
		List<Borrow> list2 = bos.readBorrowsByDate();
		model.addAttribute("books", list);
		model.addAttribute("borrows", list2);
		return "borrowManagement";
	}

	@PostMapping("/create-borrow")
	private String createBorrow(Model model, 
			@RequestParam("id") int id, 
			@RequestParam("borrower") String borrower) {
		bos.save(id, borrower);
		List<Book> list = bs.readBooks();
		List<Borrow> list2 = bos.readBorrowsByDate();
		model.addAttribute("books", list);
		model.addAttribute("borrows", list2);
		return "borrowManagement";
	}

	@PostMapping("/return-borrow")
	private String returnBorrow(Model model, 
			@RequestParam("id_borrow") int id) {
		bos.returnBorrow(id);
		List<Book> list = bs.readBooks();
		List<Borrow> list2 = bos.readBorrowsByDate();
		model.addAttribute("books", list);
		model.addAttribute("borrows", list2);
		return "borrowManagement";
	}
	
	/**
	 * Additional features
	 */

	@RequestMapping("/additional-features")
	private String additionalFeatures(Model model) {
		LocalDate today = LocalDate.now();
		model.addAttribute("today", today);
		List<Book> list = bs.readBooks();
		List<Borrow> list2 = bos.readBorrows();
		model.addAttribute("books", list);
		model.addAttribute("borrows", list2);
		return "additionalFeatures";
	}
	
	/*
	 * Request to delete / update books
	 */

	@PostMapping("delete-book")
	private String deleteBook(Model model, 
			@RequestParam("book_id") int id) {
		bs.removeBook(id);
		LocalDate today = LocalDate.now();
		model.addAttribute("today", today);
		List<Book> list = bs.readBooks();
		List<Borrow> list2 = bos.readBorrows();
		for(Borrow b : list2) {
			if(b.getBook_id() == id) {
				bos.removeBorrow(b.getId());
			}
		}
		list2 = bos.readBorrows();
		model.addAttribute("books", list);
		model.addAttribute("borrows", list2);
		return "additionalFeatures";
	}

	@PostMapping("update-book")
	private String updateBook(Model model, 
			@RequestParam("book_update_id") int id, 
			@RequestParam("title") String title,
			@RequestParam("year") int year, 
			@RequestParam("editor") String editor,
			@RequestParam("authorFirstname") String authorFirstname,
			@RequestParam("authorLastname") String authorLastname) {
		bs.updateBook(id, title, year, editor, authorFirstname, authorLastname);
		LocalDate today = LocalDate.now();
		model.addAttribute("today", today);
		List<Book> list = bs.readBooks();
		List<Borrow> list2 = bos.readBorrows();
		model.addAttribute("books", list);
		model.addAttribute("borrows", list2);
		return "additionalFeatures";
	}
	
	
	/*
	 * Request to delete / update borrows
	 */

	@PostMapping("delete-borrow")
	private String deleteBorrow(Model model, 
			@RequestParam("borrow_id") int id) {
		Borrow br = bos.load(id);
		try {
			Book b = bs.load(br.getBook_id());
			if (b.getStatus() == false)
				bs.changeStatus(b.getId());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		bos.removeBorrow(id);
		LocalDate today = LocalDate.now();
		model.addAttribute("today", today);
		List<Book> list = bs.readBooks();
		List<Borrow> list2 = bos.readBorrows();
		model.addAttribute("books", list);
		model.addAttribute("borrows", list2);
		return "additionalFeatures";
	}

	@PostMapping("update-borrow")
	private String updateBorrow(Model model, 
			@RequestParam("borrow_id") int id,
			@RequestParam("borrower") String borrower, 
			@RequestParam(required = false) String returnDate) {
		String date = returnDate;
		Borrow b = bos.load(id);
		LocalDate localDate;
		if (date != null && date !="") {
			localDate = LocalDate.parse(date);
			bs.changeStatus(b.getBook_id());
		} else {
			localDate = b.getReturnDate();
		}
		bos.updateBorrow(id, borrower, localDate);
		LocalDate today = LocalDate.now();
		model.addAttribute("today", today);
		List<Book> list = bs.readBooks();
		List<Borrow> list2 = bos.readBorrows();
		model.addAttribute("books", list);
		model.addAttribute("borrows", list2);
		return "additionalFeatures";
	}
	
	/**
	 * Generate mock date by request on /mock
	 */
	
	@RequestMapping("/mock")
	private String createData(){
		mock.createMockDataBook();
		mock.createMockDataBorrow();
		mock.updateBookStatus();
		return "home";
	}

	@Autowired
	public void setBs(BookService bs) {
		this.bs = bs;
	}

	@Autowired
	public void setBos(BorrowService bos) {
		this.bos = bos;
	}
	
	@Autowired
	public void setMock(MockDataService mock) {
		this.mock = mock;
	}
}

